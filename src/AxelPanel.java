/**
 * Created by Dmitry Vereykin on 7/26/2015.
 */
import javax.swing.*;
import java.awt.*;

public class AxelPanel extends JPanel {
    public final double SEVEN75INCH = 35;
    public final double REGULAR_COFFEE = 40;
    public final double DECAF_COFFEE = 45;

    private JRadioButton seven75Inch;
    private JRadioButton eightInch;
    private JRadioButton eight5Inch;
    private ButtonGroup bg;

    public AxelPanel() {
        setLayout(new GridLayout(3, 1));
   
        seven75Inch = new JRadioButton("7.75 inch axle", true);
        eightInch = new JRadioButton("8 inch axle");
        eight5Inch = new JRadioButton("8.5 inch axle");

        bg = new ButtonGroup();
        bg.add(seven75Inch);
        bg.add(eightInch);
        bg.add(eight5Inch);

        setBorder(BorderFactory.createTitledBorder("Axles"));

        add(seven75Inch);
        add(eightInch);
        add(eight5Inch);
    }

    public double getAxleCost() {
        double axleCost = 0.0;

        if (seven75Inch.isSelected())
            axleCost = SEVEN75INCH;
        else if (eightInch.isSelected())
            axleCost = REGULAR_COFFEE;
        else if (eight5Inch.isSelected())
            axleCost = DECAF_COFFEE;

        return axleCost;
    }
}
